
# Dragino-LGT92-fork-KiCAD-hardware

Proyecto de Hardware que forma parte del TFM para el programa del Máster Universitario de la Seguridad de la Información y las Comunicaciones, 2021-2022.

El diseño es un fork del repositorio original, cuya URL es:

https://github.com/dragino/Lora/tree/master/LGT-92

El fork del repositorio completo, se encuentra en la cuenta alternade github:

https://github.com/hwfw-wove-2b/Lora

## Cambios

Para acceder con detalle a los cambios realizados junto con git, se usa la aplicación de Cadlab:

View this project on [CADLAB.io](https://cadlab.io/project/25893). 

## Para contribuir a este reposiorio

- [ ] [Crear](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [Subir](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) archivos
- [ ] [Agregar archivos usando la línea de comandos](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) o haciendo un push de archivos preexistentes con los siguientes comandos:

```
cd existing_repo
git remote add origin https://gitlab.com/uem-tfm/dragino-lgt92-fork-kicad-hardware.git
git branch -M main
git push -uf origin main
```


## Autores
Autor de las modificaciones: Camilo Arzuza Bonett

## Licencia
(llenar la licencia CERN - OSH o creaive commons... no consigo documentación de licencia del proyecto original, en progreso)


